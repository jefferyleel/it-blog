---
title: 把檔案交給 Git 控管
date: 2020-06-26
author: jeffery
---

## 查看檔案狀態

```bash
echo "hello, git" > welcome.html    # 建立welcome.html
git status   # 查看檔案狀態(untracked file)
git add welcome.html  # 將 welcome.html 加入 git
```

## 將檔案加入 git

```bash
git add *.html   # 將副檔名 html 都加入 git
git add --all    # 將全部檔案都加入 git
git add .        # 將全部檔案都加入 git
```

## 將暫存區內容提交到倉庫裡

-  該動作可以理解為"完成存檔的動作"
- -m ‘init commit’ => 指提交的訊息，中英文都可以，只要清楚讓你之後回來看可以清楚明白

## 不一定要二段式

```bash
git commit -a -m "updatecontent"
```

> 可以透過-a縮短流程 不過對於新加入的檔案是無效的！（Untracked file）

## 什麽時候需要Commit？

1.完成⼀個「任務」的時候：不管是⼤到完成⼀整個電⼦商務的⾦流系統，還是⼩⾄只加了⼀個⾴⾯甚⾄只是改幾個字，都算是「任務」。

2.下班的時候：雖然可能還沒完全搞定任務，但⾄少先 Commit 今天的進度，除了備份之 外，也讓公司知道你今天有在努⼒⼯作。