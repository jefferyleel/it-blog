---
title: 變更檔名
date: 2020-06-26
author: jeffery
---

## 操作方法

- 直接改：
對 git 來說是兩個動作：原檔名 deleted、新檔名 untracked，在使用 git add 之後狀態會變成 renamed

- 使用 git 改
```bash
git mv <原檔名> <新檔名>
```
> 查看 status 會發現狀態直接變成 renamed

## 〖補充〗對git來說檔名不分大小寫？！

- 問題：
假設我們把 HelloWorld.html 改成 helloworld.html，對於 git 而言這兩個檔名是一樣的，所以 status不會有任何改變，之後 commit 的檔名還會是 HelloWorld.html

- 解決方法：
利用 git mv 來強制修改檔名

```bash
git mv HelloWorld.html helloworld.html
```

> 再查看 status 就可以發現檔名改變了！