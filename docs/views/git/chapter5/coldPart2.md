---
title: 在 .git ⽬錄裡有什麼東⻄？Part 2
date: 2020-06-25
author: jeffery
---

## Checkout Commit 回朔歷史版本

一個 Commit 會接連其他相關檔案更動的Commit，所以在 checkout 之後就會切換到之前的記錄

```bash
git checkout <hashId>
```

![](../images/commitTree.png)

## Git 不是做差異備份

每次 git add + git commit 完檔案之後，都會重新計算 SHA-1 數值也就是全新的 blob 物件，並不是只紀錄差異內容，所以在切換 Commit 就會像一串的歷史紀錄

![](../images/compare.png)

## 資源回收機制

Git 就算只改⼀個字也會做出⼀顆新的物件，如果有⼀個 100KB 的檔案，因為改了⼀⾏程式，就必須再做出⼀顆也差不多是 100KB 的檔案出來，雖然 Git 在製作 Blob 物件的時候已先進⾏壓縮，但還是有點浪費空間。

- 觸發時機
  - 當在 .git/objects ⽬錄的物件或是打包過的 packfile 數量過多的時候
  - 當執⾏ git push 指令把內容推⾄遠端伺服器時
  - 手動觸發 `git gc`