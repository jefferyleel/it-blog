---
title: 新增、初始 Repository
date: 2020-06-26
author: jeffery
---

## 將資料夾初始化 Git

從全新的資料夾使用 Git 來控管版本，該資料夾會建立一個 .git 資料夾(存放整個Git的精華)，如果是針對既有的資料夾只要從 Step4 輸入指令即可

```bash
Step1.  cd /tmp   # 切換⾄ /tmp ⽬錄
Step2.  mkdir git-practice  # 建立 git-practice ⽬錄
Step3.  cd git-practice   # 切換⾄ git-practice ⽬錄
Step4.  git init  # 初始化這個⽬錄，讓 Git 對這個⽬錄開始進⾏版控
```

## 移除 Git 控制

1. 只要把那個 .git ⽬錄移除，Git 就對這個⽬錄失去控制權
2. 整個專案⽬錄裡只要 .git 還在，什麼檔案或⽬錄刪了都救得回來，但 .git 只要刪掉就沒辦法

## 為何範例都要用 /tmp 目錄

/tmp ⽬錄裡的東⻄在下次電腦重新開機（或當機）的時候就全部被清空，所以不需要再⼿動整理

<Disqus/>