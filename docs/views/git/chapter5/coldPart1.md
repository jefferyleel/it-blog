---
title: 在 .git ⽬錄裡有什麼東⻄？Part 1
date: 2020-06-25
author: jeffery
---

## .git 資料夾有四種物件

- blob物件 : 存放檔案內容
就是工作目錄中某個檔案的"內容"，且只有內容而已。
當你執行 git add 指令的同時，這些新增檔案的內容就會立刻被寫入成為 blob 物件。

- tree物件 : 存放目錄以及檔案資訊
儲存特定目錄下的所有資訊，包含該目錄下的檔名、對應的blob物件名稱、檔案連結(symbolic link)或其他tree物件等等。

- commit物件 : 存放 Commit 的資訊
用來記錄有哪些tree物件包含在版本中，一個commit物件代表著Git的一次提交，記錄著特定提交版本有哪些tree物件、以及版本提交的時間、紀錄訊息等等，通常還會記錄上一層的commit物件名稱。

- tag物件 : 指向某一個 Commit 物件
是一個容器，通常用來關聯特定一個commit物件(也可以關聯到特定blob、tree物件)

![](../images/gitObjects.png)