---
title: 【狀況題】追加檔案到最近⼀次的 Commit
date: 2020-06-26
author: jeffery
---

## 方法一、使用 reset 還原在重新加入

```
1. git reset HEAD^
2. git add .
3. git commit -m 'message'
```

## 方法二、使用amend加入最後一次的commit

```
1. git add welcome.html
2. git commit --amend --no-edit
```

> 注意請不要在 commit 後已經 push 到遠端，再修改歷史訊息這樣會造成其他協作專案的人 pull 專案有問題。