---
title: 【狀況題】剛才的Commit 後悔了，想要拆掉重做
date: 2020-06-26
author: jeffery
---

## 操作方法

如果想要拆掉最後一次Commit，可以使用【相對】或【絕對】的做法

- 相對方法：
在最後⾯的那個 ^ 符號，每⼀個^符號表⽰「前⼀次」的意思，所以 e12d8ef^是指在 e12d8ef 這個 Commit 的「前⼀次」

```bash
git reset e12d8ef^
git reset HEAD^
git reset master^^
```

- 絕對方法：
先使用 git log 找尋要回去 e12d8ef 的 Commit

```bash
git reset e12d8ef
```

## Reset 分成三種模式

- mixed
是預設參數，這個模式會把暫存區的檔案丟掉，但不會動到⼯作⽬錄的檔案，也就是說 Commit 拆出來的檔案會留在⼯作⽬錄，但不會留在暫存區。

- soft
這個模式下的 reset，⼯作⽬錄跟暫存區的檔案都不會被丟掉，所以看起來就只有 HEAD 的移動⽽已。也因此，Commit 拆出來的檔案會直接放在暫存區。

- hard
在這個模式下，不管是⼯作⽬錄以及暫存區的檔案都會丟掉。

| 模式 | mixed 模式  | soft 模式  |  hard 模式 |
|:-:|:-:|:-:|:-:|:-:|
| 工作目錄 | 不變  | 不變  | 丟掉  |
| 暫存區  | 丟掉  | 不變  | 丟掉  |
