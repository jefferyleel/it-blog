---
title: 檢視紀錄
date: 2020-06-26
author: jeffery
---

可以透過 `git log` 這個指令來檢視 git 記錄，透過訊息可以看得出
1. 作者是誰
2. 什麽時候commit的
3. commit的當下做了什麽事情
4. 那個亂碼可以把它想像成 Commit 的身份證

```
$ git log
commit	cef6e4017eb1a16a7bb3434f12d9008ff83a821a (HEAD -> master) 
Author:	EddieKao <eddie@5xruby.tw> 
Date:	Wed Aug 2 03:02:37 2017 +0800

    create index page

commit	cc797cdb7c7a337824a25075e0dbe0bc7c703a1e 
Author:	Eddie Kao <eddie@5xruby.tw> 
Date:	Sun Jul	30 05:04:05 2017 +0800

    init commit
```

## 尋找某個人或某些人的 Commit

```bash
git log --oneline --author="Sherly|Eddie"
```

## 尋找特定的 Commit 內容

```bash
git log -S "Ruby"
```

## 尋找特定時間範圍內的 Commit

```bash
git log --oneline --since="9am" --until="12am" --after="2017-01"
```