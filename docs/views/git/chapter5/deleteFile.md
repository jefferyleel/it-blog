---
title: 刪除檔案
date: 2020-06-26
author: jeffery
---

## 操作方法

- 直接砍：
手動直接將檔案砍掉，並使用 git add 才會讓刪除的動作記錄起來

- 用指令砍：
```
git rm <檔名>
```
> 如果不想在被 git 管控，可以加上 --cached 參數

```
git rm <檔名> --cached
```
