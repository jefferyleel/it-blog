---
title: 【狀況題】有些檔案我不想放在 Git 裡⾯
date: 2020-06-26
author: jeffery
---

## 情境一 機密的檔案(像是aws的伺服器或第三方套件的金鑰等等)

新增.gitignore檔案，裡面新增要忽略的檔案路徑，這樣在編輯該檔案就不會被 git 追蹤

![](../images/gitignore.png)


## 情境二 已經被 git 追蹤，才寫入到 .gitignore 檔案內為何沒作用？

要先用 git rm --cached 將檔案移除 git 追蹤，在將該檔案路徑寫到 .gitignore 裡面就會有作用。
