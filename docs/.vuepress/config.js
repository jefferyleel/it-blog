const themeConfig = require('./config/theme/')

module.exports = {
	base: '/it-blog',
	head: [
    ['link', { rel: 'icon', href: 'rhino-logo.png' }],
    ['meta', { name: 'viewport', content: 'width=device-width,initial-scale=1,user-scalable=no' }],
    ['meta', { name: 'author', content: 'rhinoshield-it' }],
    ['meta', { name: 'keywords', content: 'rhinoshield-it' }],
    ['meta', { name: 'theme-color', content: '#42b983' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    ['link', { rel: 'apple-touch-icon', href: 'rhino-logo.png' }],
    ['meta', { name: 'msapplication-TileImage', content: 'rhino-logo.png' }],
    ['meta', { name: 'msapplication-TileColor', content: '#000000' }]
  ],
	theme: 'reco',
	themeConfig,
	markdown: {
    lineNumbers: true
  },
  plugins: {
    'disqus': {
      shortname: 'rhinoblog-1',
      language: 'Chinese'
    }
  }
}
