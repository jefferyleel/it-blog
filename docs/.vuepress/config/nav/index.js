module.exports = [
	{ text: 'Home', link: '/', icon: 'reco-home' },
	{ text: 'TimeLine', link: '/views/timeLine/', icon: 'reco-date' },
	{ text: 'Bitbucket', link: 'https://bitbucket.org/evolutivelabs/it-blog/src/master//', icon: 'reco-github' }
]