module.exports = {
	type: 'blog',
	logo: 'rhino-logo.png',
	search: true,
	searchMaxSuggestions: 10,
	sidebar: 'auto',
	lastUpdated: true,
	author: 'Rhinoshield IT',
	authorAvatar: 'rhino-logo.png',
	startYear: '2017',
	editLinks: true,
	mode: 'light',
	noFoundPageByTencent: false,
	// vssueConfig: {
  //   platform: 'bitbucket',
  //   owner: 'jefferyleel',
  //   repo: 'it-blog',
  //   clientId: 'LRpDu4zSrgyX4f3Duq',
	// 	clientSecret: 'G4pqYtrHnAgEDRtupNj764HWNZnxnhBr'
  // },
	blogConfig: {
		category: {
			location: 2,
			text: 'Category'
		},
		tag: {
			location: 3,
			text: 'Tag'
		}
	}
}