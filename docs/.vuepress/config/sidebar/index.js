module.exports = {
	'/views/git/': [
		{
			title: '介紹',
			collapsable: false,
			children: [
				'intro'
			]
		},
		{
			title: '開始使用Git',
			collapsable: false,
			children: [
				'chapter5/startUse',
				'chapter5/fileControl',
				'chapter5/viewLog',
				'chapter5/deleteFile',
				'chapter5/renameFile',
				'chapter5/condition1',
				'chapter5/condition2',
				'chapter5/condition3',
				'chapter5/condition4',
				'chapter5/coldPart1',
				'chapter5/coldPart2'
			]
		},
		{
			title: '使⽤分⽀',
			collapsable: false,
			children: [
				'chapter6/whyUseBranch'
			]
		}
	]
}